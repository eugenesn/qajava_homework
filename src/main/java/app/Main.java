package app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);

        System.out.println("Введите Ваше имя: ");
        String name = inp.nextLine();
        System.out.println("Введите Ваш возраст: ");
        int age = inp.nextInt();
        System.out.println("Введите размер запрашиваемого кредита: ");
        double amount = inp.nextDouble();

        String message = (!name.equals("Bob") && age >= 18 && amount <= age*100) ? "Кредит выдан" : "Кредит не выдан";

        System.out.println(message);
    }
}
